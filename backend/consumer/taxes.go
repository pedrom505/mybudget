package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

//HgKey used to access the HG API
var HgKey = "06a7586a"

//Results structure
type Results struct {
	date        string
	cdi         int
	selic       int
	dailyFactor float32
	dailySelic  float32
	dailyCDI    float32
}

//Taxes structure
type Taxes struct {
	by            string
	validKey      bool
	results       []Results
	executionTime int
	cache         bool
}

func getJSONTaxes(APIKey string) Taxes {
	var taxes Taxes

	response, err := http.Get("https://api.hgbrasil.com/finance/taxes?key=" + APIKey)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		jsonData := string(data)
		fmt.Println(jsonData)
		json.Unmarshal([]byte(jsonData), &taxes)

	}
	return taxes
}

func getCDI(HgTaxesJSON Taxes) int {

	fmt.Println(HgTaxesJSON.results)
	return 1
}

func main() {
	json := getJSONTaxes(HgKey)
	cdi := getCDI(json)
	fmt.Printf("CDI: %d\n", cdi)

	// fmt.Println("Starting the application...")
	// response, err := http.Get("https://httpbin.org/ip")
	// if err != nil {
	// 	fmt.Printf("The HTTP request failed with error %s\n", err)
	// } else {
	// 	data, _ := ioutil.ReadAll(response.Body)
	// 	fmt.Println(string(data))
	// }
	// jsonData := map[string]string{"firstname": "Nic", "lastname": "Raboy"}
	// jsonValue, _ := json.Marshal(jsonData)
	// response, err = http.Post("https://httpbin.org/post", "application/json", bytes.NewBuffer(jsonValue))
	// if err != nil {
	// 	fmt.Printf("The HTTP request failed with error %s\n", err)
	// } else {
	// 	data, _ := ioutil.ReadAll(response.Body)
	// 	fmt.Println(string(data))
	// }
	// fmt.Println("Terminating the application...")
}
